package test.com.pwootage.priortyqueue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pwootage.priortyqueue.PriorityQueue;

public class TestPriorityQueue {
	private PriorityQueue<String>	queue;
	
	@Before
	public void setUp() throws Exception {
		queue = new PriorityQueue<>();
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testCaseFromAssignment() {
		queue.enque("Sally", 2);
		queue.enque("Joe", 3);
		queue.enque("Bob", 2);
		queue.enque("John", 1);
		queue.enque("Jane", 5);
		queue.enque("Jill", 5);
		
		assertEquals("Jane", queue.removeHead());
		assertEquals("Jill", queue.removeHead());
		assertEquals("Joe", queue.removeHead());
		assertEquals("Sally", queue.removeHead());
		assertEquals("Bob", queue.removeHead());
		assertEquals("John", queue.removeHead());
		assertTrue(queue.isEmpty());
	}
	
	@Test
	public void testBackwardsPriorityFromInsert() {
		queue.enque("1", 1);
		queue.enque("2", 2);
		queue.enque("3", 3);
		queue.enque("4", 4);
		queue.enque("5", 5);
		
		assertEquals("5", queue.removeHead());
		assertEquals("4", queue.removeHead());
		assertEquals("3", queue.removeHead());
		assertEquals("2", queue.removeHead());
		assertEquals("1", queue.removeHead());
		assertTrue(queue.isEmpty());
	}
	
	@Test
	public void testAlreadyInOrderOfPriority() {
		queue.enque("5", 5);
		queue.enque("4", 4);
		queue.enque("3", 3);
		queue.enque("2", 2);
		queue.enque("1", 1);
		
		assertEquals("5", queue.removeHead());
		assertEquals("4", queue.removeHead());
		assertEquals("3", queue.removeHead());
		assertEquals("2", queue.removeHead());
		assertEquals("1", queue.removeHead());
		assertTrue(queue.isEmpty());
	}
	
	@Test
	public void testRandomOrder() {
		queue.enque("2", 2);
		queue.enque("1", 1);
		queue.enque("4", 4);
		queue.enque("5", 5);
		queue.enque("3", 3);
		
		assertEquals("5", queue.removeHead());
		assertEquals("4", queue.removeHead());
		assertEquals("3", queue.removeHead());
		assertEquals("2", queue.removeHead());
		assertEquals("1", queue.removeHead());
		assertTrue(queue.isEmpty());
	}
	
	@Test
	public void testStableOrderInOrder() {
		queue.enque("4a", 4);
		queue.enque("4b", 4);
		queue.enque("4c", 4);
		queue.enque("3", 3);
		queue.enque("2", 2);
		queue.enque("1", 1);
		
		assertEquals("4a", queue.removeHead());
		assertEquals("4b", queue.removeHead());
		assertEquals("4c", queue.removeHead());
		assertEquals("3", queue.removeHead());
		assertEquals("2", queue.removeHead());
		assertEquals("1", queue.removeHead());
		assertTrue(queue.isEmpty());
	}
	
	@Test
	public void testStableOrderBackwardsOrder() {
		queue.enque("1", 1);
		queue.enque("2", 2);
		queue.enque("3", 3);
		queue.enque("4a", 4);
		queue.enque("4b", 4);
		queue.enque("4c", 4);
		
		assertEquals("4a", queue.removeHead());
		assertEquals("4b", queue.removeHead());
		assertEquals("4c", queue.removeHead());
		assertEquals("3", queue.removeHead());
		assertEquals("2", queue.removeHead());
		assertEquals("1", queue.removeHead());
		assertTrue(queue.isEmpty());
	}
	
	@Test
	public void testStableOrderRandomOrder() {
		queue.enque("2", 2);
		queue.enque("4a", 4);
		queue.enque("1", 1);
		queue.enque("5", 5);
		queue.enque("4b", 4);
		queue.enque("3", 3);
		queue.enque("4c", 4);
		queue.enque("-1", -1);
		queue.enque("0", 0);
		
		assertEquals("5", queue.removeHead());
		assertEquals("4a", queue.removeHead());
		assertEquals("4b", queue.removeHead());
		assertEquals("4c", queue.removeHead());
		assertEquals("3", queue.removeHead());
		assertEquals("2", queue.removeHead());
		assertEquals("1", queue.removeHead());
		assertEquals("0", queue.removeHead());
		assertEquals("-1", queue.removeHead());
		assertTrue(queue.isEmpty());
	}
}
