package com.pwootage.priortyqueue;

public class PriorityQueueProgramMain {
	public static void main(String[] args) {
		PriorityQueueProgram program = new PriorityQueueProgram(System.in, System.out);
		program.run();
	}
}
