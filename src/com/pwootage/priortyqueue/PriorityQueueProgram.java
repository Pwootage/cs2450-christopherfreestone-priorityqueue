package com.pwootage.priortyqueue;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import com.pwootage.priortyqueue.input.InputHandler;
import com.pwootage.priortyqueue.input.InputItem;

public class PriorityQueueProgram {
	private InputHandler			input;
	private PrintWriter				output;
	private PriorityQueue<String>	queue;
	
	public PriorityQueueProgram(InputStream in, OutputStream out) {
		input = new InputHandler(in);
		queue = new PriorityQueue<>();
		output = new PrintWriter(out, true);
	}
	
	public void run() {
		InputItem inp = null;
		while ((inp = input.getNextInput()) != null) {
			queue.enque(inp.name, inp.priority);
		}
		while (!queue.isEmpty()) {
			output.println(queue.removeHead());
		}
	}
}
