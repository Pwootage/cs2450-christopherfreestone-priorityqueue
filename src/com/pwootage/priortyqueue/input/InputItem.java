package com.pwootage.priortyqueue.input;

public class InputItem {
	public String name;
	public int priority;
	
	public InputItem(String name, int priority) {
		this.name = name;
		this.priority = priority;
	}
}
