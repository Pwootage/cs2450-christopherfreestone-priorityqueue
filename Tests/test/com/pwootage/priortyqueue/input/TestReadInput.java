package test.com.pwootage.priortyqueue.input;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pwootage.priortyqueue.input.InputHandler;
import com.pwootage.priortyqueue.input.InputItem;

public class TestReadInput {
	private InputHandler		inputHandler;
	private PipedInputStream	pin;
	private PipedOutputStream	pout;
	private PrintWriter			out;
	
	@Before
	public void setUp() throws Exception {
		pin = new PipedInputStream();
		pout = new PipedOutputStream(pin);
		out = new PrintWriter(pout, true);
		
		inputHandler = new InputHandler(pin);
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testReadSingleValue() {
		out.println("test 1");
		InputItem inp = inputHandler.getNextInput();
		assertEquals("test", inp.name);
		assertEquals(1, inp.priority);
	}
	
	@Test
	public void testReadHugeValue() {
		String hugeName = "Thisisagiganticnamethat'sjusttoproveapointbutitcan'thaveanyspacesinit";
		int hugeNumber = 19969854;
		out.println(hugeName + " " + hugeNumber);
		
		InputItem inp = inputHandler.getNextInput();
		assertEquals(hugeName, inp.name);
		assertEquals(hugeNumber, inp.priority);
	}
	
	@Test
	public void testReadValueWithLeadingWhitespace() {
		out.println("  \t test 1");
		InputItem inp = inputHandler.getNextInput();
		assertEquals("test", inp.name);
		assertEquals(1, inp.priority);
	}
	
	@Test
	public void testReadValueWithTrailingWhitespace() {
		out.println("test 1  \t ");
		InputItem inp = inputHandler.getNextInput();
		assertEquals("test", inp.name);
		assertEquals(1, inp.priority);
	}
	
	@Test
	public void testReadBadValueWithExtraSpaces() {
		out.println("test item 1");
		
		IllegalArgumentException exception = null;
		try {
			inputHandler.getNextInput();
		} catch (IllegalArgumentException e) {
			if (e.getMessage().toLowerCase().contains("invalid input")) {
				exception = e;
			} else {
				throw e;
			}
		}
		assertNotNull("Invalid input with extra spaces did not throw exception", exception);
	}
	
	@Test
	public void testReadBadValueWithNonNumber() {
		out.println("test 1asdf");
		
		IllegalArgumentException exception = null;
		try {
			inputHandler.getNextInput();
		} catch (IllegalArgumentException e) {
			if (e.getMessage().toLowerCase().contains("invalid input")) {
				exception = e;
			} else {
				throw e;
			}
		}
		assertNotNull("Invalid input with non-number did not throw exception", exception);
	}
	
	@Test
	public void testReadMultipleValues() {
		out.println("test1 1");
		out.println("test2 2");
		out.println("test3 3");
		
		InputItem inp = inputHandler.getNextInput();
		assertEquals("test1", inp.name);
		assertEquals(1, inp.priority);
		inp = inputHandler.getNextInput();
		assertEquals("test2", inp.name);
		assertEquals(2, inp.priority);
		inp = inputHandler.getNextInput();
		assertEquals("test3", inp.name);
		assertEquals(3, inp.priority);
	}
	
	@Test
	public void testEmptyInput() {
		out.println();
		InputItem inp = inputHandler.getNextInput();
		assertNull("Somehow got an input from no input", inp);
	}
}
