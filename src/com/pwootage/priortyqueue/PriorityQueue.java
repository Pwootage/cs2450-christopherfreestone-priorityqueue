package com.pwootage.priortyqueue;

import java.util.ArrayList;
import java.util.Collections;

import com.pwootage.priortyqueue.internal.PriorityQueueItem;

/**
 * Implements a PriorityQueue by maintaining a sorted list - note that this is
 * closer to O(n log n) rather than then the optimal O(log n) due to the fact
 * I am simply resorting the list on insert and delete rather than using a
 * heap. A stable heap is fairly difficult to write, while Collections.sort()
 * is fast and stable (especially on already-sorted data, which this is)
 * 
 * @author pwootage
 * 
 * @param <T>
 *            Type of the item to store
 */
public class PriorityQueue<T> {
	private ArrayList<PriorityQueueItem<T>>	queue;
	
	public PriorityQueue() {
		queue = new ArrayList<>();
	}
	
	public void enque(T item, int priority) {
		queue.add(new PriorityQueueItem<T>(item, priority));
		Collections.sort(queue);
	}
	
	public T removeHead() {
		PriorityQueueItem<T> start = queue.remove(0);
		Collections.sort(queue);
		return start.item;
	}
	
	public boolean isEmpty() {
		return queue.size() == 0;
	}
}
