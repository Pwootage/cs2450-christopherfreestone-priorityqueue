package test.com.pwootage.priortyqueue;

import static org.junit.Assert.assertEquals;

import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pwootage.priortyqueue.PriorityQueueProgram;

public class TestPriorityQueueProgram {
	private PriorityQueueProgram	queueProgram;
	private PipedInputStream		programInputPin;
	private PipedOutputStream		programInputPout;
	private PrintWriter				inputToProgram;
	
	private PipedInputStream		programOutputPin;
	private PipedOutputStream		programOutputPout;
	private Scanner					outputFromProgram;
	
	@Before
	public void setUp() throws Exception {
		programInputPin = new PipedInputStream();
		programInputPout = new PipedOutputStream(programInputPin);
		inputToProgram = new PrintWriter(programInputPout, true);
		
		programOutputPin = new PipedInputStream();
		programOutputPout = new PipedOutputStream(programOutputPin);
		outputFromProgram = new Scanner(programOutputPin);
		
		queueProgram = new PriorityQueueProgram(programInputPin, programOutputPout);
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testFromAssignment() {
		inputToProgram.println("Sally 2");
		inputToProgram.println("Joe 3");
		inputToProgram.println("Bob 2");
		inputToProgram.println("John 1");
		inputToProgram.println("Jane 5");
		inputToProgram.println("Jill 5");
		inputToProgram.println("");
		
		queueProgram.run();
		
		assertEquals("Jane", outputFromProgram.nextLine());
		assertEquals("Jill", outputFromProgram.nextLine());
		assertEquals("Joe", outputFromProgram.nextLine());
		assertEquals("Sally", outputFromProgram.nextLine());
		assertEquals("Bob", outputFromProgram.nextLine());
		assertEquals("John", outputFromProgram.nextLine());
	}
}
