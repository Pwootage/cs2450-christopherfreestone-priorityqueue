package com.pwootage.priortyqueue.input;

import java.io.InputStream;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputHandler {
	private static final Pattern	inputPattern	= Pattern.compile("^([\\S]+) ([0-9]+)$");
	private Scanner					scanner;
	
	public InputHandler(InputStream in) {
		this.scanner = new Scanner(in);
	}
	
	public InputItem getNextInput() {
		String line = scanner.nextLine().trim();
		if (line == null) {
			throw new IllegalArgumentException("Scanner did not return a value, stream must be dead.");
		}
		if (line.equals("")) {
			return null;
		}
		Matcher m = inputPattern.matcher(line);
		if (!m.matches()) {
			throw new IllegalArgumentException("Invalid input: " + line);
		}
		String name = m.group(1);
		int priority = Integer.parseInt(m.group(2));
		return new InputItem(name, priority);
	}
	
}
