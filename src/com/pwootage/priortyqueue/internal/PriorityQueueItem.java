package com.pwootage.priortyqueue.internal;

public class PriorityQueueItem<T> implements Comparable<PriorityQueueItem<T>> {
	public int	priority;
	public T	item;
	
	public PriorityQueueItem(T item, int priority) {
		this.priority = priority;
		this.item = item;
	}
	
	@Override
	public int compareTo(PriorityQueueItem<T> o) {
		if (o.priority == this.priority) return 0;
		if (o.priority < this.priority) return -1;
		return 1;
	}
	
}
